<?php

namespace Lia\Gui;

interface Sources {

    public function get_available_themes(): array;

    public function get_available_apps(): array;

}

