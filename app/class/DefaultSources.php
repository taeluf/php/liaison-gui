<?php

namespace Lia\Gui;

/**
 * Barebones, hardcoded class that provides a list of themes and apps to setup.
 */
class DefaultSources implements Sources {

    public function __construct(\Lia\Gui\Package $gui_package){
        $this->gui_package = $gui_package;
    }

    public function get_available_themes(): array {
        $themes = json_decode(file_get_contents(dirname(__DIR__).'/available/themes.json'),true);
        return $themes;
    }

    public function get_available_apps(): array {
        $hardcoded_themes = json_decode(file_get_contents(dirname(__DIR__).'/available/apps.json'),true);
        $user_added_themes = $this->gui_package->get_settings()['available_apps'] ?? [];
        $theme_names = array_combine(array_keys($user_added_themes), array_keys($user_added_themes));
        $themes = array_merge($hardcoded_themes, $theme_names);
        return $themes;
    }
}
