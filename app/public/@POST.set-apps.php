<?php
/**
 * Enable/Disable apps, `composer require`ing and `composer remove`ing as needed.
 *
 * POST /set-apps/?apps[taeluf/user-gui]=on&apps[taeluf/liaison.seo-extractor]=on
 *
 * @TODO add validation to ensure app is in get_available_apps
 * @TODO add input validation to prevent injection
 * @warning CODE INJECTION POSSIBLE
 */


$settings = $package->get_settings();
$installed_apps = $settings['installed_apps'];
$enabled_apps = $_POST['apps'];

$apps_to_install = [];
$apps_to_remove = [];

foreach ($installed_apps as $app_name=>$app_version){
    if (!isset($enabled_apps[$app_name])){
        unset($installed_apps[$app_name]);
        $apps_to_remove[] = $app_name;
    }
}
foreach ($enabled_apps as $app_name=>$is_checbkox_on){
    if (!isset($installed_apps[$app_name])){
        $apps_to_install[] = $app_name;
        $installed_apps[$app_name] = 'version_unknown';
    }
}

$settings['installed_apps'] = $installed_apps;

$package->set_settings($settings);


$composer_dir = $package->composer_dir;

if (count($apps_to_install) > 0){
    $list_to_install = implode(" ", $apps_to_install);
    passthru("cd \"$composer_dir\"; composer require $list_to_install;");
}
if (count($apps_to_remove) > 0){
    $list_to_remove = implode(" ", $apps_to_remove);
    passthru("cd \"$composer_dir\"; composer remove $list_to_remove;");
}

