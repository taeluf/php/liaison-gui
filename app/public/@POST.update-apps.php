<?php
/**
 * Add an app to the available apps. POST set-apps/ to composer install and enable.
 *
 * POST /update-apps/ (no paramaters)
 *
 * @warning CODE INJECTION POSSIBLE through settings
 * @warning no spam protection
 * @warning no permissions protection
 */


$settings = $package->get_settings();
$composer_dir = $package->composer_dir;

passthru("cd \"$composer_dir\"; composer update;");
