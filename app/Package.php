<?php

namespace Lia\Gui;

class Package extends \Lia\Package\Server {

    /**
     * array of settings
     */
    protected array $settings;
    /**
     * Used to get apps and themes available for configuration.
     */
    public \Lia\Gui\Sources $sources;
    /**
     * Directory for the main site app. Configs, pages, views, and other items will be stored in this dir.
     */
    public string $site_dir;

    /**
     * Panel identifiers/callables
     * array<string PanelIdentifier, Callable $callable> ... Callable must return a string
     */
    public array $panels = [];

    /**
     * Directory where composer.json is stored.
     */
    public string $composer_dir;

    /**
     * absolute path to gui settings json file.
     */
    public string $settings_file;

    /**
     * Array of apps managed by Lia Gui.
     */
    public array $apps;

    public function __construct($lia, $namespace='lia:gui', $dir=__DIR__){
        parent::__construct($lia,$namespace,$dir);
    }

    /**
     * Initialize installed apps
     *  (and probably other stuff later)
     */
    public function init(){
        $settings = $this->get_settings();
        $installed = $settings['installed_apps'];
        foreach ($installed as $package=>$version){
            // $version is NOT PROPERLY SET
            $package_dir = $this->composer_dir.'/vendor/'.$package.'/';
            $this->initialize_package_dir($package_dir);
        }
    }

    /**
     * Initialize an instaled app by its directory. 
     *
     * @param dir absolute directory path to an installed app
     */
    public function initialize_package_dir(string $dir){
        // How do I initialize a package by dir?
        // I have to find the app class, or a config file, or something.
        // Then I have to initialize it.
        // So, for example, I want to add the user-gui app ... surely it needs some configuration
        // So, it'll need to add a panel link on the left side
        // and a form that allows editing of configs.
        
        $package_settings_file = $dir.'/.config/lia-gui.json';
        $json = file_get_contents($package_settings_file);
        $package_settings = json_decode($json, true);
        $class = $package_settings['class'];
        $pkg = new $class($this);
        $this->apps[] = $pkg;

        $pkg->setup_liaison($dir, $this->lia);
        // echo $pkg->something;
    }

    /**
     * Add a panel
     * @param $panel_identifier string panel identifier
     * @param Callable $callable a callable that returns an html string
     */
    public function add_panel(string $panel_identifier, Callable $callable){
        $this->panels[$panel_identifier] = $callable;
    }

    /**
     * Get an array of available panels
     * @return array<string PanelName, string PanelIdentifier>
     */
    public function get_panel_list(): array {

        $app_panels = [
            'Themes'=>'lia:gui.themes',
            'Apps'=>'lia:gui.apps',
        ];

        foreach ($this->apps as $app){
            $app_panels = array_merge($app_panels, $app->get_panel_list());
        }

        return $app_panels;
    }

    /**
     * Get panel html
     * @return string
     */
    public function get_panel(string $panel_name): string {
        $callable = $this->panels[$panel_name];
        return $callable();
    }

    /**
     * Get an array of settings for the GUI
     *
     *
     * @return array of settings
     */
    public function get_settings(): array {

        if (!isset($this->settings)){
            $file = $this->settings_file;
            $this->settings = json_decode(file_get_contents($file),true);
        }

        return $this->settings;
    }

    /**
     * Store settings to disk
     */
    public function set_settings(array $settings){
        
        $file = $this->settings_file;
        file_put_contents($file, json_encode($settings, JSON_PRETTY_PRINT));

        $this->settings = $settings;

    }
}
