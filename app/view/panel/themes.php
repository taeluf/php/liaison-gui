<?php


    $themes = $package->sources->get_available_themes();
    
    $settings = $package->get_settings();

?>

<h2>Set Theme</h2>
<form action="set-theme/" method="POST">
<select name="theme">
<?php foreach ($themes as $theme=>$description): 
    $selected = '';
    if ($theme==$settings['theme'])$selected = ' selected ';
?>
    <option value="<?=$theme?>" <?=$selected?>><?=$description?></option>
<?php endforeach; ?>
</select>

<br><br>
<input type="submit" value="Save Theme">
</form>
