<?php
/**
 * Display list of apps to install & setup.
 */


    $apps = $package->sources->get_available_apps();
    
    $settings = $package->get_settings();

?>

<h2>Update Apps</h2>
<p>Click the button to update your apps</p>
<form action="update-apps/" method="POST">
    <input type="submit" value="Update Apps (composer)">
</form>

<h2>Install Apps</h2>
<form action="install-app/" method="POST">
    <label>Composer Package Name<br>
        <input type="text" name="composer_package" placeholder="taeluf/user-gui" />
    </label>

    <br>

    <label>Composer Package Version Constraint<br>
        <input type="text" name="package_version" placeholder="v0.3.x-dev" />
    </label>


    <br>
    <input type="submit" value="Install App" />
</form>

<br><br>

<h2>Enable/Disable Apps</h2>
<form action="set-apps/" method="POST">
<?php foreach ($apps as $app=>$description): 

    $checked = '';
    if (isset($settings['installed_apps'][$app])){
        $checked = ' checked ';
    }

?>
    <label>
        <input type="checkbox" name="apps[<?=$app?>]" <?=$checked?>><?=$description?> 
    </label>
    <br>
<?php endforeach; ?>

<br><br>
<input type="submit" value="Save Apps">
</form>
