<?php  
  
class Gui extends \Lia\Addon {  

    public string $fqn = 'lia:gui.main';
      
    /**
     * Add the default panels & bootstrap & setup the server-start hook which will set the theme.
     */
    public function init_lia(){  
        $lia = $this->lia;  
        $lia->hook('ServerStart', [$this,'onServerStart']);
        require_once(__DIR__.'/../bootstrap.php');


        $this->package->add_panel('lia:gui.themes',
            [$this, 'get_themes_panel']
        );

        $this->package->add_panel('lia:gui.apps',
            [$this, 'get_apps_panel']
        );

    }  

    public function onPackageReady(){

    }  


    /**
     * Set the theme
     */
    public function onServerStart(){
        $lia = $this->lia;
        $settings = $this->package->get_settings(); 
        
        $lia->setTheme($settings['theme']);
    }

    /**
     * Return html form for configuring themes
     */
    public function get_themes_panel(): string {
        return $this->lia->view('gui:panel/themes');
    }

    /**
     * Return html form for configuring apps
     */
    public function get_apps_panel(): string {
        return $this->lia->view('gui:panel/apps');
    }
}  
