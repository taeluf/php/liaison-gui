<?php  


if ($_SERVER['REQUEST_URI']=='favicon.ico')exit;
  
// $dir = dirname(__DIR__);
$dir = __DIR__;
require_once($dir.'/vendor/autoload.php');  

$lia = new \Lia();  
  
// Add the built-in App, which provides all the web-server features.  
$server_app = new \Lia\Package\Server($lia, 'lia:server');  // dir & base_url not required  
  
// Add your app, providing home page & other core pages & features  
$gui_app = new \Lia\Gui\Package($lia);  
$gui_app->sources = new \Lia\Gui\DefaultSources($gui_app);
$gui_app->site_dir = __DIR__.'/site/';
$gui_app->composer_dir = __DIR__;
$gui_app->settings_file = __DIR__.'/settings.json';

$gui_app->init();

$lia->set('lia:server.resources.useCache', false);
  
// delivers files in `site/public/*`
$lia->deliver();  
