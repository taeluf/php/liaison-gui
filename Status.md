# Liaison GUI
End Goal: I can build a site without editing any of the code in `test/Server`, other than clearing trash created by caching, or switching code in deliver.php to a new implementation of the gui library. 

## Overview (July 1, it has since changed)
Overview of how it works currently (July 1, 2023):
- `test/Server/deliver.php` sets up liaison app & gui app
- `app/public/index.php` is home page for configuring. It has a form for setting the theme.
- `app/settings.json` contains all configurations, at least during early testing & building. 
- `app/addon/Gui.php` `require`s the `bootstrap.php` file. bootstrap requires the functions.php file
- `functions.php` defines `get_settings()` and `set_settings()`.
- `public/@POST.set-theme.php` changes the theme stored in settings.json
- `public/@POST.install-app.php` add a composer-installable app to the list of available apps
- `public/@POST.set-apps.php` enable/disable apps, calling composer require/remove as needed.
- `public/sample.php` serves as a sample page for seeing what theme is active
- `view/theme/panel.php` is used by the gui on the index page
- `view/theme/basic.php` is used as a simple example for changing the theme.

## Testing
- `vendor/bin/phptest server`
- Visit the localhost address.

## Sep 17, 2023
Installed apps can now setup panels, and have 'setup_liaison()' called during the initialization of the gui package.

see Sep 14 notes. Since we have a functional integration, it may be time to refactor and create an interface. It's still extremely feature light, but that's not much of a priority yet.

I'll need some kind of way to protect access to the panel. 

## Sep 14, 2023
added 'update-apps' route to run composer update. moved settings.json into test/Server. Also separate compoesr install in test/Server, so the full test server is self-contained, and does not pollute the actual code.

added `init()` function and init package function to Package.php. In user-gui, I added a .config/lia-gui.json file and a class to use. The package init is not functional. It initializes the class, but that's it.

Next time, I should develop out the functionality a little bit, get the panel added, get the content added to the main page. Get the app full-added so user login actually works. this will require configuring the user app, since it requires database setup and other stuff probably.

After that, it will probably be time to make an interface for apps to implement. Then I might want to start refactoring & cleaning things up. Also note that theme is set in app/addon/Gui.php, NOT in Package.php's init. Might change that idk.

## August 25, 2023
Apps can now be added from the apps panel. This simply adds them to the the settings.json, so they can be shown in the list of available apps.

Next time: For apps that are installed, I need to loop over them and load them into the site ... like so their pages actually display!

At some point, I'll need a major refactor, but that's really not a priority until it starts to come together.

## July 2, 2023
Okay. so I added class/Sources interface and class/DefaultSources with a barebones hard-coded implementation.

The overall setup now uses panels. Panels must be manually added with `$package->add_panel()`. I have a very basic themes panel and apps panel, which are defined in view/panel. the addon/Gui.php adds the panels and defines callbacks to actually load the views. `available/*` contains json files for the basic setup.

set-theme & set-app are still VERY basic.

I added a site_dir config to the package class which will be used for adding pages and storing settings, etc. I haven't actually started using it yet.

Next steps will probably be to clean up what I currently have, provide a minimal amount of validation, fix where settings are stored, .... idk.

I'll also need to actually set up the apps that have been added. Currently app setup is not standardized so i'll probably have to add an interface for any apps that can be automatically managed by the GUI.

## July 2, 2023
TODO:
- Require script to define base dir for settings, pages, and stuff.
- Add 'sources' interface. Add a base class that provides a list of themes and apps
- Add 'app' interface that provides ... panels and idk what else.
- add a way to configure each app???

SO:
The current setup is very built-in... I want to get available panels and loop over them. Every app should be able to hook into the gui and add panels.

## July 1, 2023
TODO next time:
- add configuration for adding apps
- add simple way to add pages
- start figuring out a more extensible and robust setup.

## July 1, 2023
Initialize project. Make plans.

Plans:
- Through code, setup basic liaison web server that only adds the built-in liaison app and the liaison gui app.
- visit home page (or admin page??)
- Through GUI:
    - DONE? configure theme
    - add other apps
    - add pages


I think the above plans are a good starting point. I, of course, want it to do more. However, I think this is sufficient.

Later Plans:
- Add security (so admin app is not accessible publicly)
- ... idk

